## Thrift Rpc client

使用了common-pool2连接池

## 使用

建议由服务提供者自己实现client，打包后以库包的形式供给消费者使用

服务提供者实现：
1.引入依赖
```java
<dependency>
    <groupId>com.lowdad.thrift</groupId>
    <artifactId>client</artifactId>
    <version>1.0.1</version>
</dependency>
```
2.实现clientBuilder
```java
public class K8sClientBuilder extends K8ServiceThriftClientBuilder<RpcService.Client> {

    public K8sClientBuilder(K8ServiceKey k8ServiceKey) {
        setThriftClass(RpcService.class);

        setK8ServiceKey(k8ServiceKey);
    }

    public static ThriftClient<RpcService.Client> buildClient(K8ServiceKey k8ServiceKey) {
        return new K8sClientBuilder(k8ServiceKey).build();
    }
}
```
3.实现client的自动注入
```java
@Slf4j
public class RpcClientConfiguration {

    @Value("${rpc.services.demo.host}")
    private String host;
    @Value("${rpc.services.demo.port}")
    private String port;

    @Bean(name = "demoClient")
    @ConditionalOnMissingBean(name = "demoClient")
    public ThriftClient<RpcService.Client> k8ServiceThriftClient() {
        log.info("[RPC] demoClient configuration");
        K8ServiceKey k8ServiceKey = new K8ServiceKey(host, Integer.valueOf(port));
        log.info("[RPC] demoClientClient key:" + k8ServiceKey);
        return K8sClientBuilder.buildClient(k8ServiceKey);
    }
}
```
4.增加自动注册规则
在resources包下新建META-INF包，加入spring.factories文件
```properties
org.springframework.boot.autoconfigure.EnableAutoConfiguration=dev.javawcy.microlibs.demo.client.config.RpcClientConfiguration
```


消费者实现：

1.首先导入服务提供的jar,这里本地我采用了maven引入
```java
<dependency>
    <groupId>dev.javawcy.microlibs</groupId>
    <artifactId>demo-client</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```
2.启动类中加入扫描路径便于springboot扫描到自动注册的信息
```java
@SpringBootApplication
@ComponentScan(value = {"com.lowdad.thrift","dev.javawcy.microlibs.clientdemo"})
public class ClientDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientDemoApplication.class, args);
    }

}
``` 
3.使用autowire引入使用
```java
@RestController
@Slf4j
public class demoController {

    @Autowired
    private ThriftClient<RpcService.Client> demoClient;

    @PostMapping("ping")
    public String Ping() {
        log.info("call");
        return demoClient.call(cli -> cli.Ping("client-demo"));
    }
}
```
4.可用的配置

```yaml
rpc:
  client:
    maxFrameSize: 16777216
    timeOut: 5000
    pool:
      maxConn: 1024
      minIdle: 8
      maxIdle: 32
    threadPool:
      coreSize: 10
      maxSize: 100
      keepAlive: 10
  services:
    demo:
      host: 127.0.0.1
      port: 9000
```

traceId参考trace项目介绍，enjoy

## License MIT
