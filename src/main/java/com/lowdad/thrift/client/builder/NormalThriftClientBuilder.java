package com.lowdad.thrift.client.builder;

import com.lowdad.thrift.client.NormalThriftClient;
import org.apache.thrift.TServiceClient;

/**
 * @author lowdad
 */
public class NormalThriftClientBuilder<TCLIENT extends TServiceClient> {

    private final NormalThriftClient<TCLIENT> client = new NormalThriftClient<>();

    protected NormalThriftClient<TCLIENT> build() {
        client.init();
        return client;
    }

    protected NormalThriftClientBuilder<TCLIENT> setHost(String host) {
        client.setThriftServerHost(host);
        return this;
    }

    protected NormalThriftClientBuilder<TCLIENT> setPort(int port) {
        client.setThriftServerPort(port);
        return this;
    }

    protected NormalThriftClientBuilder<TCLIENT> setThriftClass(Class<?> thriftClass) {
        client.setThriftClass(thriftClass);
        return this;
    }
}