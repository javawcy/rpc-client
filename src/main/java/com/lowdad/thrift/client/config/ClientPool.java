package com.lowdad.thrift.client.config;

/**
 * @author lowdad
 */
public class ClientPool {
    private String maxConn;
    private String minIdle;
    private String maxIdle;

    public String getMaxConn() {
        return maxConn;
    }

    public void setMaxConn(String maxConn) {
        this.maxConn = maxConn;
    }

    public String getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(String minIdle) {
        this.minIdle = minIdle;
    }

    public String getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(String maxIdle) {
        this.maxIdle = maxIdle;
    }
}
