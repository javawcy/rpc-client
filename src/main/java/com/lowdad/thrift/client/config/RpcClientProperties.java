package com.lowdad.thrift.client.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author lowdad
 */
@Component
@ConfigurationProperties(prefix = "rpc.client")
public class RpcClientProperties {
    private String timeOut;
    private String maxFrameSize;
    private ClientPool pool;
    private ClientThreadPool threadPool;

    private static RpcClientProperties config;

    public static RpcClientProperties getInstance() {
        return config;
    }

    @PostConstruct
    public void init() {
        config = this;
        config.timeOut = this.timeOut;
        config.maxFrameSize = this.maxFrameSize;
        config.pool = this.pool;
        config.threadPool = this.threadPool;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getMaxFrameSize() {
        return maxFrameSize;
    }

    public void setMaxFrameSize(String maxFrameSize) {
        this.maxFrameSize = maxFrameSize;
    }

    public ClientPool getPool() {
        return pool;
    }

    public void setPool(ClientPool pool) {
        this.pool = pool;
    }

    public ClientThreadPool getThreadPool() {
        return threadPool;
    }

    public void setThreadPool(ClientThreadPool threadPool) {
        this.threadPool = threadPool;
    }
}
