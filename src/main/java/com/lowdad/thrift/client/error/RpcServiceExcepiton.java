package com.lowdad.thrift.client.error;

/**
 * @author lowdad
 */
public class RpcServiceExcepiton extends RuntimeException {

    public RpcServiceExcepiton() {
    }

    public RpcServiceExcepiton(String message) {
        super(message);
    }

    public RpcServiceExcepiton(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcServiceExcepiton(Throwable cause) {
        super(cause);
    }

    public RpcServiceExcepiton(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
