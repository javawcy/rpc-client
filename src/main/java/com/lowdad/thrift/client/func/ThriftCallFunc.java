package com.lowdad.thrift.client.func;

import org.apache.thrift.TServiceClient;

/**
 * @author lowdad
 */
public interface ThriftCallFunc<TCLIENT extends TServiceClient, TRET> {

    TRET call(TCLIENT tcall) throws Exception;

}