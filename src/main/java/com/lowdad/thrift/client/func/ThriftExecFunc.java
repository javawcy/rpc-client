package com.lowdad.thrift.client.func;

import org.apache.thrift.TServiceClient;

/**
 * @author lowdad
 */
public interface ThriftExecFunc<TCLIENT extends TServiceClient> {

    void exec(TCLIENT tclient) throws Exception;

}