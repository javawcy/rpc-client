package com.lowdad.thrift.client.pool;

import com.lowdad.thrift.client.K8ServiceKey;
import com.lowdad.thrift.client.config.RpcClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericKeyedObjectPool;
import org.apache.thrift.transport.TTransport;

/**
 * @author lowdad
 */
@Slf4j
public class TTransportPool extends GenericKeyedObjectPool<K8ServiceKey, TTransport> {


    public TTransportPool(TTransportPoolFactory factory) {
        super(factory);

        setTimeBetweenEvictionRunsMillis(45 * 1000);
        setNumTestsPerEvictionRun(5);
        setMaxWaitMillis(30 * 1000);

        setMaxTotal(Integer.valueOf(RpcClientProperties.getInstance().getPool().getMaxConn()));
        setMaxTotalPerKey(Integer.valueOf(RpcClientProperties.getInstance().getPool().getMaxConn()));
        log.info("[RPC] client pool maxConn {}",RpcClientProperties.getInstance().getPool().getMaxConn());
        setMinIdlePerKey(Integer.valueOf(RpcClientProperties.getInstance().getPool().getMinIdle()));
        log.info("[RPC] client pool minIdle {}",RpcClientProperties.getInstance().getPool().getMinIdle());
        setMaxTotalPerKey(Integer.valueOf(RpcClientProperties.getInstance().getPool().getMaxIdle()));
        log.info("[RPC] client pool maxIdle {}",RpcClientProperties.getInstance().getPool().getMaxIdle());

        setTestOnCreate(true);
        setTestOnBorrow(true);
        setTestWhileIdle(true);
    }

    @Override
    public TTransportPoolFactory getFactory() {
        return (TTransportPoolFactory) super.getFactory();
    }

    public void returnBrokenObject(K8ServiceKey key, TTransport transport) {
        try {
            invalidateObject(key, transport);
        } catch (Exception e) {
            log.warn("[RPC] client pool return broken key " + key);
            e.printStackTrace();
        }
    }
}